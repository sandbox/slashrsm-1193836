<?php
/**
 * @file
 * per_node_comment_moderation.features.inc
 */

/**
 * Implementation of hook_views_api().
 */
function per_node_comment_moderation_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}
